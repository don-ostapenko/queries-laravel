<?php

function isSubDomain(string $domain)
{
    $parsedUrl = parse_url($domain);

    if (!empty($parsedUrl['scheme'])) {
        $domain = str_replace($parsedUrl['scheme'] . '://', '', $domain);
    }

    $res = extractSubDomain($domain);

    if (!empty($res)) {
        return true;
    } else {
        return false;
    }
}

function extractSubDomain($domain)
{
    $subDomain = $domain;
    $domain = extractDomain($subDomain);
    $subDomain = rtrim(strstr($subDomain, $domain, true), '.');

    return $subDomain;
}

function extractDomain($domain)
{
    if (preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i", $domain, $matches)) {
        return $matches['domain'];
    } else {
        return $domain;
    }
}
